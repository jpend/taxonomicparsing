class TaxonomicParse
# If time permits, revise the class, add attributes and better objects within the class.
# Does the single @ symbol work? CHECK LATER!

    # Define and load xmlfile
    def initialize() #bib
            @ipni = readipni(File.expand_path('../ipni.txt', __FILE__)) ;nil
            # @bib =
            # @xmlfile =
    end

    attr_reader :ipni

    def ipnitest(x, ipniindex)
    # Use this function to implement IPNI testing of journal names. It relies on supplemetary functions, built outside this class.
        if x[0].journal.nil? == false && ipniindex.nil? == false && x[0].journal[0].gsub(/[^a-z0-9\s]/i, '') != @ipni[ipniindex][2].gsub(/[^a-z0-9\s]/i, '') # Get raw journal names for proper comparison

            # puts "=====Check journal name! (1) Anystyle: #{x[0].journal[0]} is not (2) IPNI: #{ipni[ipniindex][2]}!====="
            # puts "=========Type 2 to accept IPNI name!========="

            decision = 1 # gets.chomp.to_i # User input
            if decision == 2
                x[0].journal = @ipni[ipniindex][2]
                puts "I took IPNI name!"
            end
        end
        return(x)
    end

    def refparse(xmlfile, refs, bib)
        refs.each do |refs|
            puts ("Parsing #{refs.text}")

            x = Anystyle.parse(refs.text.delete("\n"), format = :bibtex) # Doing the actual parsing of the text between the reference tags. Removes new lines within reference tags (e.g., 1142, 361 of vol 7)
            
            # next if (x[0].respond_to?(:'unmatched-author') == true || x[0].respond_to?(:'unmatched-volume') == true || x[0].respond_to?(:'unmatched-journal') == true) 
            # Skip references that have more than one reference contained within. Deal with this later.
                puts "SEPARATE REFS in #{xmlfile}"
                next
            end

            ipniindex = ipnimatch(@ipni, refs.text) ;nil # matching in ipnimatch.rb

            x = ipnitest(x, ipniindex) # Testing against IPNI. Skip if ref is a book or if there are no IPNI matches. Get rid of punctuation for a clean comparison.

            b = bib.query() {|o| o.digest == x[0].digest} # Query the bib to see if the ref is already there

            if (b.empty? == false)
                xmlref = BibTeX.parse(b[0].to_s).to_xml(:extended => true)  # Convert to BibTeXML from bibtex, to collect all the author information that might be useful. xml file should reflect the best case entry ID
                puts "I appended 1 #{b[0]}"
            else
                bib << x.to_s # Save it in the bibliography
                xmlref = BibTeX.parse(bib[bib.length-1].to_s).to_xml(:extended => true)
                puts "I appended 2 #{bib[bib.length-1]}"
            end
            puts "refparse bib length is #{bib.length}"

            refdoc = Nokogiri::XML(xmlref.to_s).remove_namespaces!  # Convert to Nokogiri XML

            entry = refdoc.at_xpath('//entry')

            entry.children[0].children[0].add_previous_sibling "<verbatimtext>#{refs.text}</verbatimtext>" # Add verbatim text
            entry.children[0].add_previous_sibling "<referenceid>#{Digest::MD5.hexdigest(refs.text)}</referenceid>" # Add checksum (i.e., reference id)

            linkup = LinkRetrieve.new(refs.text, x) # This is a class where I've stored my methods to connect to BHL and PubMed
            links = linkup.reflink # Get links if DOI exists
            linkup.appendlinks(links, entry) # If links exist, append to XML file.

            @doc.root << refdoc.xpath("//entry").to_s # Append to master XML file. refdoc encoding is fine.
        end

        nf = File.basename(xmlfile,File.extname(xmlfile)).concat("_refs.xml") # Create new file name
        File.open(nf, "w"){ |f| f.puts @doc.to_xml } # Save results to a file
        return bib
    end


    def refget(xmlfile, bib)
        # This function reads in the XML file, calls the parser, and returns the new BibTeX library (i.e., the bib).
        refs = File.open(xmlfile) { |f| Nokogiri::XML(f) } .xpath("//reference") # Read the file into a string variable and pull out the references
        if (refs.length > 0) # Only parse and create file if there are references in the treatment XML
            @doc = Nokogiri::XML('<references/>', nil, 'UTF-8') # Create an empty document to append references.
            puts "refget bib length is #{bib.length}"
            refparse(xmlfile, refs, bib)
        else
            puts ("Skipping file. No references contained within...")
        end
        return bib
    end

    def fileparse(wd, globalbib, begreq = nil, endreq = nil)
        # This function navigates to the folder you are working on, receives the global bib, and starts at a requested file. It returns the new bib.
        # Add feature to turn off prompting, to simply run through completely (without IPNI checks).

        search = Dir.glob("#{Dir.home}/#{wd}/*.xml") # Find the XML files for parsing

        grepstring = search.collect {|s| s.encode("UTF-16be", :invalid=>:replace, :replace=>"?").encode('UTF-8')} # Remove encoding problems. Make file list amenable to grep.

        if (endreq.nil? == false)
            endrange = grepstring.index{|s| s =~ /#{endreq}/} # Create a list of files. Start at the index of the file you want to begin with.
        else
            endrange = search.length-1
        end

        list = search[grepstring.index{|s| s =~ /#{begreq}/}..endrange]

        puts "fileparse bib length is #{globalbib.length}"

        list.each do |x|
            puts "Reading file #{x}"
            globalbib = refget(x, globalbib) # Run the reference parsing script.
        end

        return globalbib
    end 
end
