def append(wd)
	# Call this function to bulk run idlist
	
	list = Dir.glob("#{Dir.home}/#{wd}/refs/*_refs.xml") # Find the XML files for parsing

    list.each do |x|
        puts "Reading file #{x}"
        idlist(x, wd) # Run the reference parsing script.
    end
end


def idlist(refsfile, wd)
# Read in the refs file.
# Append list of entry ids to original treatment XML.

    require 'nokogiri'

	xmlfile = File.basename(refsfile,File.extname(refsfile))[0..-6].concat(".xml") 
	
    pdoc = File.open("#{Dir.home}/#{wd}/#{xmlfile}") { |f| Nokogiri::XML(f) } # read in the post-Charparser xml file
    rdoc = File.open(refsfile) { |f| Nokogiri::XML(f) } # read in the references, to obtain correct entry ids

    pdoc.at_xpath("//references").add_next_sibling "<references_links/>" # add a references_links node. bio namespace. fine for now but fix later!
    rdoc.xpath("//@id").collect { |x| pdoc.at_xpath("//bio:references_links").add_child("<reference_link>#{x.to_s}</reference_link>") } # add ids as children

    nf = File.basename(xmlfile,File.extname(xmlfile)).concat("_links.xml") # Create new file name
    File.open(nf, "w"){ |f| f.puts pdoc.to_xml } # Save results to a file

end