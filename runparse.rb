def runparse(wd, inputbib, outputbib, endreq = nil, begreq = nil)

    # Enter the end request as the file you wish to finish with.

    require 'nokogiri'
    require 'anystyle/parser'
    require 'bibtex/ruby'

    $LOAD_PATH << '.' # Load Ruby process' current working directory
    require 'readipni.rb' # Dependencies
    require 'ipnimatch.rb'
    require 'linkretrieve.rb'
    require 'taxonomicparse.rb' # Require for later use

    bib = BibTeX.open("#{Dir.home}/#{inputbib}") # Change this?

    folder = "#{Dir.home}/#{wd}/refs"  # create folder and move them to the folder so that you can zip them!
    if (Dir.exist?(folder) == false) then Dir.mkdir(folder) end
    Dir.chdir(folder)
    # Dir.chdir("#{Dir.home}/#{wd}/refs") # Change this? Won't work with Dir.pwd
    # mkdir refs. cd there.


    parseobj = TaxonomicParse.new
    bib = parseobj.fileparse(wd, bib, begreq, endreq)
    bib.save_to("#{Dir.home}/#{outputbib}") # Change this?

    Dir.chdir("#{Dir.home}/taxonomicparsing/")

end
