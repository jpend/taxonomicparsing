def readipni(file)

    array = File.read(file).split("\n")
    newarray = []

    array.each_with_index do |row, index|
        newarray[index] = row.split("%") # array within an array to get the rows
    end  

    set = []
    newarray.each_with_index do |row, index|
        if row[2].nil? || row[2].to_s == ""
            # puts "Deleting row #{index} because of empty abbreviation!"
            set.push(index)# doesn't work
        end
    end
    
     newarray.delete_if.with_index { |_, index| set.include? index }

    return newarray
end