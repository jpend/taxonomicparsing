# README #

For a detailed document on why, what and how this code was developed, see https://docs.google.com/document/d/1juSmXYtQKA6qMOymMbMpY9dUG-QcdedBTWZKmLd9Zrk/edit?usp=sharing

### What is this repository for? ###

* These scripts **parse taxonomic references** found in XML treatment files, under the tag <references>.
* Version 1

### How do I get set up? ###

* Install Ruby v.1.9.3 or greater.
* Install anystyle-parser `sudo gem install anystyle-parser`
* Install facets `sudo gem install facets`

#### Now you are ready to run the code, ####
1. Create a folder called ```refs``` in your working directory (i.e., where your files to be processed are stored).
2. Navigate to the folder ```taxonomicparsing``` where the Ruby parsing code is stored.
3. Enter an irb session, load runparse.rb and supply arguments (see below).
3a. Supply a **working directory, input bibliography and a file name for an output bibliography**. You can also specify which file to begin parsing with and which file to stop parsing at (begreq, endreq). The file specified by endreq will be the last file to be parsed. If begreq nor endreq is specified, all files in a directory are processed.

```
irb
load 'runparse.rb'
runparse('unzipped_volumes/Caryophyllidae_no_keys/no_keys', 'bibtexlibrary.bib', 'bibtexlibrary_Caryophyllidae.bib',  endreq='V5_1243')
```
The resulting bibtex library will be stored in your home directory.

If you wish to train the parser with the taxonomic datasets provided,

```
irb
require 'anystyle/parser'
Anystyle.parser.train 'trainingdata/trainvol7.txt', true
Anystyle.parser.model.save
load 'runparse.rb'
runparse('unzipped_volumes/v2_no_keys/no_keys', 'bibtexlibrary.bib', 'newvol2.bib')
```

If you wish to use the associated XML processing scripts to prepare your parsed references for import into a Semantic MediaWiki (SMW), use the following script.

```
irb
load 'transformxml/transformvol.rb'
transform('etc_processing_files/FNA_Github_June_15_2016/FNA_Github_June_15_2016/V5','V5')
```

#### Dependencies ####
    * nokogiri
    * anystyle-parser
    * bibtex-ruby

#### How do I run tests? ####
By modifying the source code

### XML file transformation code ###

Additional code was developed to transform output XML files containing parsed references into SMW ready-to-import, one-file-per-volume XML files. This code can be found in ```transformvol.rb```.

### Contribution guidelines ###

* Feel free to re-purpose this code!

### Who do I talk to? ###

* Repo owner: Jocelyn Pender (pender.jocelyn@gmail.com)
* Other team contact: Joel Sachs (joel.sachs@agr.gc.ca)