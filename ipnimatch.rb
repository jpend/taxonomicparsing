def ipnimatch(ipni, refstext)

    saveindex = []
    saveabb = []

    ipni.each_with_index do |row, index|
        begin # begin the task. (there is a rescue later on)
            if (refstext.delete("\n").split.join(" ") =~ /#{row[2]}/) # strict matching has to be on the other side of the equation.
                puts "Found it! Abbreviation is #{row[2]}; index is #{index}"
                saveindex.concat([index])
                saveabb.concat([row[2]])
            end
        rescue
        end
    end ;nil

    # need to save these options, then select the longest one!
    if saveabb != []
        ipnijournal = saveabb.max_by(&:length)
        ipniindex = saveindex[saveabb.index(ipnijournal)]
    end

    return ipniindex

end