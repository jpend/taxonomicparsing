<xsl:stylesheet
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
exclude-result-prefixes="xs">

	<xsl:output method="xml" version="1.0"
	encoding="UTF-8"/>

	<xsl:template match="/">
		<mediawiki xmlns="http://www.mediawiki.org/xml/export-0.8/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mediawiki.org/xml/export-0.8/ http://www.mediawiki.org/xml/export-0.8.xsd" version="0.8" xml:lang="en">
			<siteinfo>
				<sitename>FNA</sitename>
				<base>http://fna.biowikifarm.net/wiki/Main_Page</base>
				<generator>MediaWiki 1.23.6</generator>
				<case>case-sensitive</case>
				<namespaces>
<namespace key="-2" case="case-sensitive">Media</namespace><namespace key="-1" case="first-letter">Special</namespace><namespace key="0" case="case-sensitive"/><namespace key="1" case="case-sensitive">Talk</namespace><namespace key="2" case="first-letter">User</namespace><namespace key="3" case="first-letter">User talk</namespace><namespace key="4" case="case-sensitive">Project</namespace><namespace key="5" case="case-sensitive">Project talk</namespace><namespace key="6" case="case-sensitive">File</namespace><namespace key="7" case="case-sensitive">File talk</namespace><namespace key="8" case="first-letter">MediaWiki</namespace><namespace key="9" case="first-letter">MediaWiki talk</namespace><namespace key="10" case="case-sensitive">Template</namespace><namespace key="11" case="case-sensitive">Template talk</namespace><namespace key="12" case="case-sensitive">Help</namespace><namespace key="13" case="case-sensitive">Help talk</namespace><namespace key="14" case="case-sensitive">Category</namespace><namespace key="15" case="case-sensitive">Category talk</namespace><namespace key="102" case="case-sensitive">Property</namespace><namespace key="103" case="case-sensitive">Property talk</namespace><namespace key="106" case="case-sensitive">Form</namespace><namespace key="107" case="case-sensitive">Form talk</namespace><namespace key="108" case="case-sensitive">Concept</namespace><namespace key="109" case="case-sensitive">Concept talk</namespace><namespace key="170" case="case-sensitive">Filter</namespace><namespace key="171" case="case-sensitive">Filter talk</namespace><namespace key="900" case="case-sensitive">Reference</namespace><namespace key="901" case="case-sensitive">Reference talk</namespace><namespace key="902" case="case-sensitive">Author</namespace><namespace key="903" case="case-sensitive">Author talk</namespace><namespace key="904" case="case-sensitive">Tree</namespace><namespace key="905" case="case-sensitive">Tree talk</namespace>
				</namespaces>
			</siteinfo>
			<page>
				<xsl:apply-templates select="*" />
			</page>
		</mediawiki>
	</xsl:template>

	<xsl:template match="entry">
		<title><xsl:value-of select="@id"/></title>
			<ns>900</ns>
			<revision>
			<text xml:space="preserve">
		{{Reference
		| entryid=<xsl:value-of select="@id"/>
		| titleid=Reference:<xsl:value-of select="@id"/>
		<xsl:apply-templates select="*" />
		}}
</text>
	<model>wikitext</model>
	<format>text/x-wiki</format>
</revision>
	</xsl:template>

	<xsl:template match="referenceid">
		| checksum=<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="verbatimtext">| verbatimtext=<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="author">
		| author=<xsl:for-each select="person">
			<xsl:if test="position() &lt; last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>;
				</xsl:if><xsl:if test="position() = last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if></xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="editor">
		| editor=<xsl:for-each select="person">
			<xsl:if test="position() &lt; last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>;
				</xsl:if><xsl:if test="position() = last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if></xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="translator">
		| translator=<xsl:for-each select="person">
			<xsl:if test="position() &lt; last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>;
				</xsl:if><xsl:if test="position() = last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if></xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="//entry/*/*[not(self::author) and not(self::editor) and not(self::translator) and not(.='')]">		| <xsl:value-of select ="local-name()"/>=<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="BHLlink">
		| BHLlink=<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="PubMedlink">
		| PubMedlink=<xsl:value-of select="."/>
	</xsl:template>
</xsl:stylesheet>
