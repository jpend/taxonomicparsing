<xsl:stylesheet
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
exclude-result-prefixes="xs">

	<xsl:output method="xml" version="1.0"
	encoding="UTF-8"/>

	<xsl:template match="person">
			<page><title>
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if>
				<xsl:choose>
					<xsl:when test="last and first">
						<xsl:value-of select="last"/>, <xsl:value-of select="first"/>
					</xsl:when>
					<xsl:when test="not(last) and first">
						<xsl:value-of select="first"/>
					</xsl:when>
					<xsl:when test="last and not(first)">
						<xsl:value-of select="last"/>
					</xsl:when>
		 		</xsl:choose>
				<xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>
			</title>
				<ns>902</ns>
				<revision>
				<text xml:space="preserve">
			{{Author
			| author=<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
			<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
			<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>
			}}</text>
		<model>wikitext</model>
		<format>text/x-wiki</format>
	</revision></page>
		</xsl:template>

</xsl:stylesheet>
