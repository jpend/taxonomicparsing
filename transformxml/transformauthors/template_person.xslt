<xsl:stylesheet
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
exclude-result-prefixes="xs">

	<xsl:output method="xml" version="1.0" encoding="UTF-8"/>

	<xsl:template match="/">
<mediawiki xmlns="http://www.mediawiki.org/xml/export-0.10/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mediawiki.org/xml/export-0.10/ http://www.mediawiki.org/xml/export-0.10.xsd" version="0.10" xml:lang="en">
			<siteinfo><sitename>FNA</sitename><dbname>fna</dbname><base>http://localhost:8080/w/index.php/Main_Page</base><generator>MediaWiki 1.29.1</generator><case>case-sensitive</case><namespaces><namespace key="-2" case="case-sensitive">Media</namespace><namespace key="-1" case="first-letter">Special</namespace><namespace key="0" case="case-sensitive"/><namespace key="1" case="case-sensitive">Talk</namespace><namespace key="2" case="first-letter">User</namespace><namespace key="3" case="first-letter">User talk</namespace><namespace key="4" case="case-sensitive">Project</namespace><namespace key="5" case="case-sensitive">Project talk</namespace><namespace key="6" case="case-sensitive">File</namespace><namespace key="7" case="case-sensitive">File talk</namespace><namespace key="8" case="first-letter">MediaWiki</namespace><namespace key="9" case="first-letter">MediaWiki talk</namespace><namespace key="10" case="case-sensitive">Template</namespace><namespace key="11" case="case-sensitive">Template talk</namespace><namespace key="12" case="case-sensitive">Help</namespace><namespace key="13" case="case-sensitive">Help talk</namespace><namespace key="14" case="case-sensitive">Category</namespace><namespace key="15" case="case-sensitive">Category talk</namespace><namespace key="102" case="case-sensitive">Property</namespace><namespace key="103" case="case-sensitive">Property talk</namespace><namespace key="106" case="case-sensitive">Form</namespace><namespace key="107" case="case-sensitive">Form talk</namespace><namespace key="108" case="case-sensitive">Concept</namespace><namespace key="109" case="case-sensitive">Concept talk</namespace><namespace key="170" case="case-sensitive">Filter</namespace><namespace key="171" case="case-sensitive">Filter talk</namespace><namespace key="900" case="case-sensitive">Reference</namespace><namespace key="901" case="case-sensitive">Reference talk</namespace><namespace key="902" case="case-sensitive">Author</namespace><namespace key="903" case="case-sensitive">Author talk</namespace><namespace key="904" case="case-sensitive">Tree</namespace><namespace key="905" case="case-sensitive">Tree talk</namespace>
	</namespaces>
</siteinfo>
				<xsl:apply-templates select="//entry/*/*/person" />
		</mediawiki>
	</xsl:template>

	<xsl:template match="person">
			<page xmlns="http://www.mediawiki.org/xml/export-0.10/"><title>
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if>
				<xsl:choose>
					<xsl:when test="last and first">
						<xsl:value-of select="last"/>, <xsl:value-of select="first"/>
					</xsl:when>
					<xsl:when test="not(last) and first">
						<xsl:value-of select="first"/>
					</xsl:when>
					<xsl:when test="last and not(first)">
						<xsl:value-of select="last"/>
					</xsl:when>
		 		</xsl:choose>
				<xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>
			</title>
				<ns>902</ns>
				<revision>
				<text xml:space="preserve">
			{{Author
			| author=<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
			<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
			<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>
			}}</text>
		<model>wikitext</model>
		<format>text/x-wiki</format>
	</revision></page>
		</xsl:template>

</xsl:stylesheet>
