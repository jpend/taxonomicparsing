<xsl:stylesheet
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
exclude-result-prefixes="xs">

	<xsl:output method="xml" version="1.0"
	encoding="UTF-8"/>

	<xsl:template match="/">
			<page>
				<xsl:apply-templates select="*" />
			</page>
	</xsl:template>

	<xsl:template match="entry">
		<title><xsl:value-of select="@id"/></title>
			<ns>900</ns>
			<revision>
			<text xml:space="preserve">
		{{Reference
		| entryid=<xsl:value-of select="@id"/>
		| titleid=Reference:<xsl:value-of select="@id"/>
		<xsl:apply-templates select="*" />
		}}
</text>
	<model>wikitext</model>
	<format>text/x-wiki</format>
</revision>
	</xsl:template>

	<xsl:template match="referenceid">
		| checksum=<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="verbatimtext">| verbatimtext=<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="author">
		| author=<xsl:for-each select="person">
			<xsl:if test="position() &lt; last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>;
				</xsl:if><xsl:if test="position() = last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if></xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="editor">
		| editor=<xsl:for-each select="person">
			<xsl:if test="position() &lt; last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>;
				</xsl:if><xsl:if test="position() = last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if></xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="translator">
		| translator=<xsl:for-each select="person">
			<xsl:if test="position() &lt; last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if>;
				</xsl:if><xsl:if test="position() = last()">
				<xsl:if test="prelast"><xsl:value-of select="prelast"/>&#160;</xsl:if><xsl:choose><xsl:when test="last and first"><xsl:value-of select="last"/>, <xsl:value-of select="first"/></xsl:when>
				<xsl:when test="not(last) and first"><xsl:value-of select="first"/></xsl:when>
				<xsl:when test="last and not(first)"><xsl:value-of select="last"/></xsl:when></xsl:choose><xsl:if test="lineage">, <xsl:value-of select="lineage"/></xsl:if></xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="//entry/*/*[not(self::author) and not(self::editor) and not(self::translator) and not(.='')]">		| <xsl:value-of select ="local-name()"/>=<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="BHLlink">
		| BHLlink=<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="PubMedlink">
		| PubMedlink=<xsl:value-of select="."/>
	</xsl:template>
</xsl:stylesheet>
