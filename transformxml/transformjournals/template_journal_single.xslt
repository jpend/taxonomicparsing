<xsl:stylesheet
xmlns:xs="http://www.w3.org/2001/XMLSchema"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
exclude-result-prefixes="xs">

	<xsl:output method="xml" version="1.0"
	encoding="UTF-8"/>

	<xsl:template match="entry">
			<page><title>
			Test
			</title>
				<ns>900</ns>
				<revision>
				<text xml:space="preserve">
			{{Journal
			| journal=
					<xsl:value-of select="journal"/>
			}}</text>
		<model>wikitext</model>
		<format>text/x-wiki</format>
	</revision></page>
		</xsl:template>

</xsl:stylesheet>
