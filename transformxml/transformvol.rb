def transform(folder,volumename)
    require 'nokogiri'
    voldir = Dir.new(Dir.home.concat("/#{folder}")) # find the volumes
    template = Nokogiri::XSLT(File.read(Dir.home.concat('/taxonomicparsing/transformxml/template.xslt'))) # template based on one reference
    pagetemp = Nokogiri::XSLT(File.read(Dir.home.concat('/taxonomicparsing/transformxml/template_single.xslt'))) # page template

    num = 0 # keep track of how many references you need in your final file!
    Dir.glob("#{voldir.path}*/*_refs.xml").each_with_index do |file, index|
        puts "Doing #{file}"
        document = Nokogiri::XML(File.read(file))
        if document.xpath("*").children.length > 0 # some documents are empty because all the references were doubled refs
            reftag = document.xpath("//entry")

             # use the full template for the first ref of first doc.
            if index == 0

            @doc = template.transform(Nokogiri::XML(reftag[0].to_s, nil, 'UTF-8'))
            num = num + 1
            end

            if (index==0 && reftag.length > 1) then reftag.shift end# remove first one, already stored above.
            if (index==0 && reftag.length > 1) || index > 0 # more than one reference? add pages

                trans = reftag.collect{|x| pagetemp.transform(Nokogiri::XML(x.to_s, nil, 'UTF-8'))} #transform all the tags
                num = num + trans.length

                pages = trans[0] # initialize this variable
                trans.each do |x|
                    pages = pages.xpath('//page') << x.root # append the files
                end

                # puts pages.to_xml
                page = @doc.at_css "page" # WHY IS ONLY CSS WORKING?? FIX!!
                page.add_next_sibling pages
            end
        end
    end
    # Dir.chdir(Dir.home)
    File.open(volumename.concat("_pages.xml"), 'w').write(@doc) # save finished product
    puts "Volume xml file should have #{num} references!"
end
