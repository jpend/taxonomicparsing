class LinkRetrieve

    require 'net/http'
    require 'nokogiri'
    require 'json'
    require 'facets'

    def initialize(s, x)
            @s = s
            @x = x
    end

    attr_reader :s, :x

    def apitest(doc)
        if (doc[0]["title"] != nil)
            if (doc[0]["title"].similarity(@x[0].title.to_s) > 0.9) # take first DOI automatically, only if the score is 100
                doi = doc[0]["doi"].gsub("http://dx.doi.org/", '') # crossref API always matches *something*. Find a way to only grab DOIs if it is an exact match!

                # BHL #
                # doi = '10.2307/3391978'
                bhluri = URI("http://www.biodiversitylibrary.org/api2/httpquery.ashx?op=GetPartByIdentifier&type=doi&value=#{doi}&apikey=7b749cbe-fdbe-4872-b5f2-55ce7082fab2")
                bhlfile = Net::HTTP.get(bhluri)
                bhlurl = Nokogiri::XML(bhlfile).xpath('//PartUrl').text

                # PubMed #
                # doi = '10.3732/ajb.93.4.607'
                pmuri = URI("http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=#{doi}")
                pmfile = Net::HTTP.get(pmuri)
                pmid = Nokogiri::XML(pmfile).xpath('//Id').text # Get PMID using DOI
                if (pmid.length > 0) then pubmedurl = "http://www.ncbi.nlm.nih.gov/pubmed/#{pmid}" else pubmedurl = "" end

                puts "Found links!"
                return([bhlurl, pubmedurl])
            else
                puts "No DOI hits in BHL or PubMed. Returning nil."
                return nil
            end
        else
            puts "No DOI hits in BHL or PubMed. Returning nil."
            return nil
        end
    end

    def reflink() # Takes the ref string and an anystyle bibtex parsed ref as input
        # DOI retrieval #
        # s = 'Al-Shehbaz Price. 1998. Delimitation of the genus Nasturtium (Brassicaceae). Novon 8: 124–126.'
        text = @s.gsub(/[^a-z0-9\s]/i, ' ').split(" ").join("+") # Only grab text

        uri = URI("http://search.crossref.org/dois?q=#{text}")
        file = Net::HTTP.get(uri) ;nil
        if (file != "<h1>Internal Server Error</h1>" && file != '') 
            puts "Found DOI. Searching for BHL or PubMed links..."
            doc = JSON.parse(file)
            links = apitest(doc)
        else
            puts "No DOI match. Returning nil."
        end
        return links
    end

    def appendlinks(links, entry)
        if (links.nil? == false)
                if (links[0].empty? == false) then entry.add_child "<BHLlink>#{links[0]}</BHLlink>" end
                if (links[1].empty? == false) then entry.add_child "<PubMedlink>#{links[1]}</PubMedlink>" end
                puts "Appending to XML file."
        end
    end

end
